

import UIKit

enum ParamKeys : String {
    
    case id = "id"
    case name = "name"
    case username = "username"
    case email = "email"
    case address = "address"
    case street = "street"
    case suite = "suite"
    case city = "city"
    case zipcode = "zipcode"
    case geo = "geo"
    case lat = "lat"
    case lng = "lng"
    
    case phone = "phone"
    case website = "website"
    case company = "company"
    case userId = "userId"
    case title = "title"
    
    case completed = "completed"
    
    case albumId = "albumId"
    case url = "url"
    case thumbnailUrl = "thumbnailUrl"
    case body = "body"
    
    
}


