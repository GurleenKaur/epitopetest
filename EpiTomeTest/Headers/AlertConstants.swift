//
//  AlertConstants.swift
//  MbKutz
//
//  Created by cbl24 on 24/04/17.
//  Copyright © 2017 codebrew. All rights reserved.
//

import UIKit
typealias AlertOkAction = () -> ()
enum AlertConstantsKeys : String {
    
    case markComplete = "mark this appointment complete"
    case cancel = "cancel this appointment"

}
