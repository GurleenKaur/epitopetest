//
//  Constants.swift
//  HutchDecor
//
//  Created by Aseem 13 on 14/09/16.
//  Copyright © 2016 Taran. All rights reserved.
//

import UIKit
import SwiftyJSON

enum Alert : String{
    case success = "Success"
    case oops = "Error "
    case login = "Login Successfull"
    case ok = "Ok"
    case cancel = "Cancel"
    case error = "Error"
    case newAppointment = "You have a new request for a service"
    case sorry = "Sorry"
    case emp = ""
    
}

enum TitleEnum : String{
    
    case noDesc = "No Description"
    
    var localized: String {
        return NSLocalizedString(self.rawValue, comment: "")
    }
}


infix operator =>
infix operator =|
infix operator =<
infix operator =||
infix operator =^^


typealias OptionalJSON = [String : JSON]?

func =>(key : ParamKeys, json : OptionalJSON) -> String? {
    return json?[key.rawValue]?.stringValue ?? ""
}

func =<(key : ParamKeys, json : OptionalJSON) -> [String : JSON]? {
    return json?[key.rawValue]?.dictionaryValue
}

func =|(key : ParamKeys, json : OptionalJSON) -> [JSON]? {
    return json?[key.rawValue]?.arrayValue
}

func =||(key : ParamKeys, json : OptionalJSON) -> Int? {
    return json?[key.rawValue]?.intValue
}

func =^^(key : ParamKeys, json : OptionalJSON) -> Float? {
    return json?[key.rawValue]?.floatValue
}



//MARK:- PROTOCOL
protocol OptionalType { init() }

//MARK:- EXTENSIONS
extension String: OptionalType {}
extension Int: OptionalType {}
extension Double: OptionalType {}
extension Bool: OptionalType {}
extension Float: OptionalType {}
extension CGFloat: OptionalType {}
extension CGRect: OptionalType {}
extension UIImage: OptionalType {}
extension IndexPath: OptionalType {}

prefix operator /

//unwrapping values
prefix func /<T: OptionalType>( value: T?) -> T {
    guard let validValue = value else { return T() }
    return validValue
}


//prefix operator /
//prefix func /(value : String?) -> String {
//    return value.unwrap()
//}

enum SettingsDetails : String{
    
    case editProfile = "Edit Profile"
    case changePassword = "Change password"
    case managePayment = "Manage payment"
    
    case reportAProblem = "Report a problem"
    case privacyPolicy = "Privacy policy"
    case termsOfService = "Terms of service"
    case logout = "Logout"
    
    
    var idArray : String{
        return self.rawValue
    }
}

