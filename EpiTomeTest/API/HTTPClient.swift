//
//  HTTPClient.swift
//  MbKutz
//
//  Created by Aseem 13 on 15/12/16.
//  Copyright © 2016 Taran. All rights reserved.
//

import Foundation
import Alamofire

typealias HttpClientSuccess = (Any? , Int) -> ()
typealias HttpClientFailure = (String) -> ()


class HTTPClient {
    
    func JSONObjectWithData(data: NSData) -> Any? {
        do { return try JSONSerialization.jsonObject(with: data as Data, options: []) }
        catch { return .none }
    }
    
    func postRequest(withApi api : Router  , success : @escaping HttpClientSuccess , failure : @escaping HttpClientFailure , header: [String: String] ){
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        var fullPath = APIConstants.basePath + api.route
        let params = api.parameters
      
        let method = api.method
        print(header)
        print(fullPath)
        print(params as Any)
       
        Alamofire.request(fullPath, method: method, parameters: params, encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            switch response.result {
            case .success(let data):
                success(data , response.response?.statusCode ?? 0)
                
            case .failure(let error):
                print(error.localizedDescription)
                failure(error.localizedDescription)
            }
        }
    }

}


func isConnectedToNetwork() -> Bool {
    guard let reachability = Alamofire.NetworkReachabilityManager()?.isReachable else { return false }
    return reachability ? true: false
}

