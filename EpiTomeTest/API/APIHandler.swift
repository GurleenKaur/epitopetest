

import Foundation
import SwiftyJSON

enum ResponseKeys : String {
    
   
    case data = "data"
   
    case msg = "msg"
   
}



extension HomeEndpoint {

    func handle(parameters : JSON) -> Any? {
        
        switch self {
            
        case .getAlbums(_):
            return AlbumsToDo.getAlbumsCollection(array: parameters.arrayValue)
            
        case .getPhotos(_):
            return Photos.getAlbumsCollection(array: parameters.arrayValue)
            
        case .getTodo(_):
            return AlbumsToDo.getAlbumsCollection(array: parameters.arrayValue)
            
        case .getPosts(_):
            return AlbumsToDo.getAlbumsCollection(array: parameters.arrayValue)
            
        case .getUsers:
            return User.getCuserCollection(array: parameters.arrayValue)
        }
    }

}



