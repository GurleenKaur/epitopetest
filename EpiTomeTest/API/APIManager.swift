


import Foundation
import SwiftyJSON
import NVActivityIndicatorView
import Alamofire
 
typealias Completion = (Response) -> ()

class APIManager : UIViewController, NVActivityIndicatorViewable{
    
    
    static let shared = APIManager()
    private lazy var httpClient : HTTPClient = HTTPClient()
    
    func request(with api : Router , isLoaderNeeded: Bool? = true , completion : @escaping Completion , header: [String: String] )  {
        
        if !isConnectedToNetwork(){
            Alerts.shared.show(alert: .oops, message:  "You need to enable internet connection" , type: .error)
            return
        }
        
        if isLoaderNeeded ?? true {
            startAnimating(CGSize(width: 40 , height: 40 ), message: nil, messageFont: nil, type: .ballSpinFadeLoader, color: #colorLiteral(red: 0.3363157809, green: 0.1373911202, blue: 0.5032842755, alpha: 1) , padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
        }
        
        httpClient.postRequest(withApi: api, success: {[weak self] (data , statusCode) in
            
            self?.stopAnimating()
            
            guard let response = data else {
                completion(Response.failure(.none))
                return
            }
            let json = JSON(response)
            print(json)

            var responseType = Validate(rawValue: json[APIConstants.status].intValue) ?? .failure
            if statusCode == 200{
                responseType = Validate.success
            }
            
            if json[APIConstants.status].intValue == Validate.notFound.rawValue{
                completion( Response.success("") )
            }
            
            if json[APIConstants.status].intValue == Validate.failure.rawValue{
                completion(Response.failure( json[APIConstants.message].stringValue ))
            }
            if json[APIConstants.status].intValue == Validate.sessionExpire.rawValue{
//                self?.tokenExpired(isTokenExpire: false)
                return
            }
        
            
            if responseType == Validate.success{
                
                let object : Any?
                object = api.handle( parameters: json )
                completion( Response.success(object) )
                
                return
            }else{
                completion(Response.failure( json[APIConstants.message].stringValue )) }
            
            }, failure: {[weak self] (message) in
                
                self?.stopAnimating()
                completion(Response.failure( message ))
                
        }, header: header)
        
    }
    
    
}

