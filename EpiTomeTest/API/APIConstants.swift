

import Foundation


internal struct APIConstants {
    
    static let basePath = "https://jsonplaceholder.typicode.com/"
    static let users = "users"
    static let posts = "posts"
    static let album = "albums"
    static let todo = "todos"
    static let photos = "photos"
    static let status = "status"
    static let message = "message"
}

enum Keys : String{
    
    
    
    //getPostOfUser
    case userId = "userId"
    static let postsUserOriented : [Keys] = [.userId]
    
    //get hotos of album
    case albumId = "albumId"
    static let photosOfAlbum : [Keys] = [.albumId]
    
}

enum Validate : Int {
    
    case none
    case success = 201
    case notFound = 404
    case failure = 400
    case sessionExpire = 401
    case invalidAccessToken = 2
    case fbLogin = 3
    
    
    func map(response message : String?) -> String? {
        
        switch self {
        case .success:
            return message
        case .failure :
            return message
            
        default:
            return nil
        }
        
    }
}

enum Response {
    case success(Any?)
    case failure(Any?)
}

typealias OptionalDictionary = [String : Any]?



