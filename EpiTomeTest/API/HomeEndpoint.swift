//
//  Home.swift
//  MbKutz
//
//  Created by Aseem 13 on 15/12/16.
//  Copyright © 2016 Taran. All rights reserved.
//

import UIKit
import Alamofire

enum HomeEndpoint{
    case getUsers()
    case getPosts(userId: String?)
    case getAlbums(userId: String?)
    case getTodo(userId: String?)
    case getPhotos(albumId: String?)
    
}

extension HomeEndpoint : Router {
    
    func request(isImage: Bool, images: [UIImage?]?, isLoaderNeeded: Bool?, header: [String : String], completion: @escaping Completion) {
        APIManager.shared.request(with: self, isLoaderNeeded: isLoaderNeeded, completion: completion, header: header)
    }
    
    
    var route : String  {
        
        switch self {
        case .getUsers(): return APIConstants.users
        case .getPosts(_): return APIConstants.posts
        case .getAlbums(_): return APIConstants.album
        case .getTodo(_): return APIConstants.todo
        case  .getPhotos(_): return APIConstants.photos
            
        }
    }
    
    var parameters: OptionalDictionary {
        return format()
    }
    
    func format() -> OptionalDictionary {
        
        switch self {
            
        case .getUsers():
            return [:]
            
        case .getPosts( let userId) , .getTodo( let userId)  , .getAlbums( let userId)  :
            return Keys.postsUserOriented.map(values: [ userId ])
            
        case .getPhotos( let albumId):
            return Keys.photosOfAlbum.map(values: [ albumId ])
            
        }
    }
    
    var method : Alamofire.HTTPMethod {
        return .get
    }
    
    var baseURL: String {
        return APIConstants.basePath
    }
    
}


