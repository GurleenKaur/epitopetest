//
//  User.swift
//  EpiTomeTest
//
//  Created by Harminder on 06/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject {
    
    var id: String?
    var name: String?
    var username: String?
    var email: String?
    var website: String?
    var phone: String?
    var address: UserAddress?
    
    init(attributes: OptionalJSON) {
        super.init()
        id = .id => attributes
        name = .name => attributes
        username = .username => attributes
        email = .email => attributes
        website = .website => attributes
        phone = .phone => attributes
        let adr = .address =< attributes
        address = UserAddress(attributes: adr)
    }
    
    class func getCuserCollection(array : [JSON]) -> [User]? {
        var tempArr : [User] = []
        for dict in array {
            tempArr.append(User(attributes: dict.dictionaryValue))
        }
        return tempArr
    }
    
}


class UserAddress: NSObject {
    
    var street: String?
    var suite: String?
    var city: String?
    var zip: String?
    var geo: AddressGeo?
    
    init(attributes: OptionalJSON) {
        super.init()
        street = .street => attributes
        suite = .suite => attributes
        city = .city => attributes
        zip = .zipcode => attributes
        let geoLoc = .geo =< attributes
        geo = AddressGeo(attributes: geoLoc)
    }
    
    
}

class AddressGeo : NSObject {
    
    var lat:String?
    var long: String?
    
    init(attributes: OptionalJSON) {
        super.init()
        lat = .lat => attributes
        long = .lng => attributes
    }
    
    override init() {
        super.init()
    }
}

class AlbumsToDo: NSObject {
    
    var id: String?
    var userId: String?
    var title: String?
    var todoCompleted: String?
    var body: String?
    
    
    init(attributes: OptionalJSON) {
        super.init()
        id = .id => attributes
        userId = .userId => attributes
        title = .title => attributes
        todoCompleted = .completed => attributes
        body = .body => attributes
    }
    
    class func getAlbumsCollection(array : [JSON]) -> [AlbumsToDo]? {
        var tempArr : [AlbumsToDo] = []
        for dict in array {
            tempArr.append(AlbumsToDo(attributes: dict.dictionaryValue))
        }
        return tempArr
    }
    
}


class Photos: NSObject {
    
    var id: String?
    var albumId: String?
    var title: String?
    var url: String?
    var thumbUrl: String?
    
    
    init(attributes: OptionalJSON) {
        super.init()
        albumId = .albumId => attributes
        id = .id => attributes
        url = .url => attributes
        title = .title => attributes
        thumbUrl = .thumbnailUrl => attributes
    }
    
    class func getAlbumsCollection(array : [JSON]) -> [Photos]? {
        var tempArr : [Photos] = []
        for dict in array {
            tempArr.append(Photos(attributes: dict.dictionaryValue))
        }
        return tempArr
    }
    
}

