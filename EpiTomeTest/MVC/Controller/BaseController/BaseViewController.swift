//
//  BaseViewController.swift
//  MAC
//
//  Created by cbl24 on 26/10/17.
//  Copyright © 2017 Codebrew. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    //MARK::- PROPERTIES
    
    var ScrollViewListener : ScrollViewDidScroll?
    var tableViewDataSource: TableViewDataSource?
    var collectionViewDataSource : CollectionViewDataSource?
  
    var collectionViewItems = [Any]()
    var item = [Any]()
    var cellForItemAt: ListCellConfigureBlock?
    var cellForRowAt: ListCellConfigureBlock?
    var didSelectRow : DidSelectedRow?
    var willDisplayCell: WillDisplayTableViewCellBlock?
    var viewForHeader: ViewForHeaderInSection?
    var viewForCollectionHeader : ViewForHeaderInSectionCollectionView?
    var headerHeight : CGFloat = 0.0
    var edgeInset: UIEdgeInsets?
    var interItemSpace: CGFloat?
    var lineSpacing: CGFloat?
    var headerCollectionViewSize: CGSize?
    
    
    enum ScreenTypes{
        case generic
        case user
    }
    
    
    //MARK::- VIEW CYCLE
    
    override func viewDidLoad() {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    //MARK::- CONFIGURE TABLEVIEW
    
    func configureTableView(tableView: UITableView , cellIdentifier: [CellIdentifiers] , screenType: ScreenTypes , rowHeight : CGFloat = 64 ){
        
        switch screenType {
            
        case .generic:
            tableViewDataSource = TableViewDataSource(items: item, height: rowHeight, tableView: tableView, cellIdentifier: cellIdentifier.first)
            tableViewDataSource?.configureCellBlock = cellForRowAt
            tableViewDataSource?.aRowSelectedListener = didSelectRow
            tableViewDataSource?.viewforHeaderInSection = viewForHeader
            tableViewDataSource?.headerHeight = headerHeight
            tableView.dataSource = tableViewDataSource
            tableView.delegate = tableViewDataSource
            tableView.reloadData()
            
        case .user:
            tableViewDataSource = UserTableViewDataSource(items: item, height: rowHeight, tableView: tableView, cellIdentifier: cellIdentifier.first)
            tableViewDataSource?.configureCellBlock = cellForRowAt
            tableViewDataSource?.aRowSelectedListener = didSelectRow
            tableViewDataSource?.viewforHeaderInSection = viewForHeader
            tableViewDataSource?.headerHeight = headerHeight
            tableView.dataSource = tableViewDataSource
            tableView.delegate = tableViewDataSource
            tableView.reloadData()
            
        }
        
    }
    
    //MARK::- CONFIGURE COLLECTIONVIEW
    func configureCollectionView(collectionView: UICollectionView , cellIdentifier: [CellIdentifiers] , screenType: ScreenTypes , cellSize : CGSize){
        switch screenType {
        case .generic:
            collectionViewDataSource = CollectionViewDataSource(items: collectionViewItems, collectionView: collectionView, cellIdentifier: cellIdentifier.first.map { $0.rawValue } , headerIdentifier: "", cellHeight: cellSize.height, cellWidth: cellSize.width, minimumLineSpacing: lineSpacing ?? 0.0, minimumInteritemSpacing: interItemSpace ?? 0.0, edgeInsetsMake: edgeInset ??  UIEdgeInsets.zero, sectionHeaderHeight: headerCollectionViewSize ?? CGSize.zero)
            collectionViewDataSource?.configureCellBlock = cellForItemAt
            collectionViewDataSource?.aRowSelectedListener = didSelectRow
            collectionView.dataSource = collectionViewDataSource
            collectionView.delegate = collectionViewDataSource
            collectionView.reloadData()
            
        case .user:
            break
       
        }
    }
    
    
    //END EDITING
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    
    
}
