//
//  AlbumViewController.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class AlbumViewController: BaseViewController {

    //MARK::- OUTLETS
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK::- PROPERTIES
    var albumId: String?
    
    
    //MARK::- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        retrieveUserInfo()
    }

    //MARK::- ACTIONS
    

    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}

extension AlbumViewController {
    
    //RETRIEVEING ALBUM'S PHOTOS
    func retrieveUserInfo(){
        HomeEndpoint.getPhotos(albumId: /albumId).request(isImage: false, images: [], isLoaderNeeded: true , header: [:]) { [ weak self ]  (response) in
            switch response {
            case .success(let pics):
                guard let phtos = pics as? [Photos] else { return }
                self?.collectionViewItems = phtos
                self?.configureCollectionView()
            case .failure(let str):
                Alerts.shared.show(alert: .oops, message:  str as? String , type: .error)
            }
        }
    }
}

//MARK::- CONFIGURE COLLECTION VIEW
extension AlbumViewController{
    
    func configureCollectionView(){
        
        cellForItemAt = { ( cell , item , indexPath) in
            guard let cell = cell as? AlbumSPhotoCollectionViewCell else { return }
            cell.photo = item
        }
        
        configureCollectionView(collectionView: collectionView, cellIdentifier: [.albumSPhotoCollectionViewCell], screenType: .generic, cellSize: CGSize(width: UIScreen.main.bounds.width/2.1 , height: UIScreen.main.bounds.width/2.1 ))
        
    }
    
}
