//
//  PostViewController.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class PostViewController: UIViewController {

    //MARK::-OUTLETS
    
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var labelHeader: UILabel!
    
    //MARK::- PROPERTIES
    var post: AlbumsToDo?
    
    //MARK:- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        updatePostInfo()
    }
    
    //MARK::- FUNCTIONS
    func updatePostInfo(){
        textViewDesc?.text = /post?.body?.capitalizedFirstLetter
        labelHeader?.text = /post?.title?.capitalizedFirstLetter
    }
    
    //MARK::- ACTIONS
    
    @IBAction func btnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
