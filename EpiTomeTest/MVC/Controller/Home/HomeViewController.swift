//
//  HomeViewController.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class HomeViewController: BaseViewController {
    
    //MARK::- OUTLETS
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK::- PROPERTIS
    enum Details: Int{
        case post = 0
        case album = 1
        case todo = 2
    }
    var todo = [Any]()
    var post = [Any]()
    var album = [Any]()
    var users = [Any]()
    let postVc = StoryboardScene.Main.postViewController.instantiate()
    
    
    //MARK::- VIEW CYCLE
    override func viewDidLoad() {
        super.viewDidLoad()
        retrieveUserInfo()
        
    }
    
    //MARK::- ACTIONS
    
    
}


//MARK::- FUNCTIONS
extension HomeViewController{
    
    
    
}

//MARK::- API HANDLER
extension HomeViewController{
    
    //RETRIEVEING USERS LIST
    func retrieveUserInfo(){
        HomeEndpoint.getUsers().request(isImage: false, images: [], isLoaderNeeded: true , header: [:]) { [ weak self ]  (response) in
            switch response {
            case .success(let users):
                guard let userList = users as? [User] else { return }
                self?.labelNoData.isHidden = false
                self?.users = userList
                self?.collectionViewItems = userList
                self?.configureCollectionView()
            case .failure(let str):
                Alerts.shared.show(alert: .oops, message:  str as? String , type: .error)
            }
        }
    }
    
    func handleSuccess(of infoType: Details , response : Any , userId : String){
        switch infoType {
        case .album:
            guard let userAlbum = response as? [AlbumsToDo] else { return }
            self.album = userAlbum
            proceedWithStatus(of: .todo, userId: userId)
        case .post:
            guard let userPost = response as? [AlbumsToDo] else { return }
            self.post = userPost
            proceedWithStatus(of: .album, userId: userId)
        case .todo:
            guard let todos = response as? [AlbumsToDo] else { return }
            self.todo = todos
            configureTableView()
        }
    }
    
    func handleResponse(response : Response , phase: Details , userId: String) {
        
        switch response{
            
        case .success(let responseValue):
            handleSuccess(of: phase, response: responseValue, userId: userId )
            
        case .failure(let str):
            Alerts.shared.show(alert: .oops, message:  str as? String , type: .error)
        }
    }
    
    func proceedWithStatus(of infoType: Details , userId: String? ){
        
        switch infoType {
            
        case .album:
            HomeEndpoint.getAlbums(userId: userId).request(isImage: false, images: [], isLoaderNeeded: true, header: [:], completion: { (response) in
                self.handleResponse(response : response , phase: infoType , userId: /userId)
            })
            
        case .post:
            HomeEndpoint.getPosts(userId: /userId ).request(isImage: false, images: [], isLoaderNeeded: true, header: [:], completion: { (response) in
                self.handleResponse(response : response , phase: infoType , userId: /userId)
            })
            
        case .todo:
            HomeEndpoint.getTodo(userId: userId).request(isImage: false, images: [], isLoaderNeeded: true, header: [:], completion: { (response) in
                self.handleResponse(response : response , phase: infoType, userId: /userId )
            })
        }
        
        
        
        
    }
    
}





//MARK::- CONFIGURE TABEVIEW , COLLECTIONVIEW , CELL DELEGATES
extension HomeViewController  {
    
  
    
    func configureTableView(){
        labelNoData.isHidden = self.post.count != 0
        
        headerHeight = 0.0
        didSelectRow = { [ weak self ] indexPath in
            switch indexPath.section{
            case 0:
                self?.postVc.post = self?.post[indexPath.row] as? AlbumsToDo
                self?.navigationController?.pushViewController(self?.postVc ?? UIViewController(), animated: true)
            
            case 2:
                if !(/((self?.todo[indexPath.row] as? AlbumsToDo)?.todoCompleted)?.toBoolVal()){
                    (self?.todo[indexPath.row] as? AlbumsToDo)?.todoCompleted = "true"
                    (self?.tableViewDataSource as? UserTableViewDataSource)?.album  = self?.todo ?? []
                }
                self?.tableView.reloadData()
            default:
                break
            }
        }
        
        cellForRowAt = { [ weak self ] ( cell , item , indexPath) in
            (cell as? UserDetailsTableViewCell)?.obj = item
            if indexPath.section == 2{
                (cell as? UserDetailsTableViewCell)?.labelInfo.textColor = /(item as? AlbumsToDo)?.todoCompleted?.toBoolVal() ?  #colorLiteral(red: 0.1960784346, green: 0.3411764801, blue: 0.1019607857, alpha: 1) : #colorLiteral(red: 0.5725490451, green: 0, blue: 0.2313725501, alpha: 1)
            }else{
                (cell as? UserDetailsTableViewCell)?.labelInfo.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            }
        }
        
        configureTableView(tableView: self.tableView, cellIdentifier: [.userDetailsTableViewCell], screenType: .user, rowHeight: UITableViewAutomaticDimension)
        (tableViewDataSource as? UserTableViewDataSource)?.album = self.album
        (tableViewDataSource as? UserTableViewDataSource)?.posts = self.post
        (tableViewDataSource as? UserTableViewDataSource)?.todos = self.todo
        self.tableView.reloadData()
    }
    
    
    func configureCollectionView(){
        
        didSelectRow = { [ weak self ] indexPath in
            let users = self?.users[indexPath.row] as? User
            self?.proceedWithStatus(of: .post , userId: /users?.id )
        }
        
        cellForItemAt = { ( cell , item , indexPath) in
            guard let cell = cell as? EnlistUsersCollectionViewCell else { return }
            cell.user = item
        }
        
        configureCollectionView(collectionView: collectionView, cellIdentifier: [.enlistUsersCollectionViewCell], screenType: .generic, cellSize: CGSize(width: 140 , height: 140 ))
        
    }
    
}
