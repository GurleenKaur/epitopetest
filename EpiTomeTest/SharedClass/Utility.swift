//
//  Utility.swift
//  MbKutz
//
//  Created by Aseem 13 on 15/12/16.
//  Copyright © 2016 Taran. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class Utility: UIViewController, NVActivityIndicatorViewable {
    
    static let functions = Utility()
    
    func loader()  {
        startAnimating(CGSize(width: 40 , height: 40 ), message: nil, messageFont: nil, type: .ballSpinFadeLoader, color: #colorLiteral(red: 0.1827337742, green: 0.8590830564, blue: 0.6430129409, alpha: 1) , padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil)
    }
    
    func removeLoader()  {
        stopAnimating()
    }
    
    
    
}

func handleAuthorization(vc: Any){
    //    let alertController = UIAlertController (title: "", message: "Go to Settings?", preferredStyle: .alert)
    //
    //    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
    //
    guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
        return
    }
    
    if UIApplication.shared.canOpenURL(settingsUrl) {
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                print("Settings opened: \(success)")
            })
        } else {
            UIApplication.shared.openURL(settingsUrl)
            
        }
    }
    //    }
    //    alertController.addAction(settingsAction)
    //    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
    //    alertController.addAction(cancelAction)
    //    (vc as? UIViewController)?.present(alertController, animated: true, completion: nil)
    //    (vc as? ESTabBarController)?.present(alertController, animated: true, completion: nil)
}
