//
//  AlertsClass.swift
//  MbKutz
//
//  Created by Aseem 13 on 15/12/16.
//  Copyright © 2016 Taran. All rights reserved.
//

import UIKit
import ISMessages

typealias AlertBlock = (_ success: AlertTag) -> ()

enum AlertTag {
    case done
    case yes
    case no
}

class Alerts: NSObject {
    
    static let shared = Alerts()
    
    func runThisAfterDelay(seconds: Double, after: @escaping () -> Void) {
        runThisAfterDelay(seconds: seconds, queue: DispatchQueue.main, after: after)
    }
    
    func runThisAfterDelay(seconds: Double, queue: DispatchQueue, after: @escaping () -> Void) {
        let time = DispatchTime.now() + Double(Int64(seconds * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        queue.asyncAfter(deadline: time, execute: after)
    }
    
    func show(alert title : Alert , message : String? , type : ISAlertType){
        
        ISMessages.hideAlert(animated: true)
        
        ISMessages.showCardAlert(withTitle: title.rawValue , message: /message , duration: 0.1 , hideOnSwipe: true , hideOnTap: true , alertType: type , alertPosition: .bottom , didHide: nil)
        runThisAfterDelay(seconds:3.0) {
            ISMessages.hideAlert(animated: true)
        }
        
    }
    
}
