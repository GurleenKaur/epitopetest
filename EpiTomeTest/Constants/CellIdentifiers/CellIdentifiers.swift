//
//  CellIdentifiers.swift
//  GasItUp
//
//  Created by cbl24_Mac_mini on 21/02/18.
//  Copyright © 2018 cbl24_Mac_mini. All rights reserved.
//

import UIKit


//swiftgen strings -t swift3 "$PROJECT_DIR/MAC/Localize/en.lproj/Localizable.strings"  --output "$PROJECT_DIR/MAC/Constants/LocalizedConstant.swift"
//swiftgen storyboards -t swift3 "$PROJECT_DIR" --output "$PROJECT_DIR/GasItUp/Constants/StoryBoardConstant.swift"


enum CellIdentifiers : String {
    case enlistUsersCollectionViewCell = "EnlistUsersCollectionViewCell"
    case userDetailsTableViewCell = "UserDetailsTableViewCell"
    case albumTableViewCell = "AlbumTableViewCell"
    case albumCollectionViewCell = "AlbumCollectionViewCell"
    case albumSPhotoCollectionViewCell = "AlbumSPhotoCollectionViewCell"
}
