//
//  AlbumSPhotoCollectionViewCell.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class AlbumSPhotoCollectionViewCell: UICollectionViewCell {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var imageAlbum: UIImageView!
    
    //MARK::- PROPERTIES
    var photo: Any? {
        didSet{
            guard let pics = photo as? Photos else { return }
            imageAlbum.kf.setImage(with: URL(string: /pics.thumbUrl))
        }
    }
}
