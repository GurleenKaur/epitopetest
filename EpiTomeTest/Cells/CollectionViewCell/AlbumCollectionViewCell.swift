//
//  AlbumCollectionViewCell.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class AlbumCollectionViewCell: UICollectionViewCell {
    
    //MARK::- OUTLETS
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageAlbum: UIImageView!{
        didSet{
            let gradient = CAGradientLayer()
            gradient.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
            gradient.colors = [    UIColor(red:18/255, green:27/255, blue:64/255, alpha:0.5).cgColor,    UIColor(red:168/255, green:28/255, blue:95/255, alpha:0.5).cgColor]
            gradient.locations = [0, 1]
            gradient.startPoint = CGPoint(x: 0, y: 0.5)
            gradient.endPoint = CGPoint(x: 1, y: 0.5)
            gradient.cornerRadius = 0
            imageAlbum.layer.insertSublayer(gradient, at: 0)
        }
    }
    
    //MARK::- PROPERTIES
    var album: Any?{
        didSet{
            guard let albumDet = album as? AlbumsToDo else { return }
            labelTitle.text = albumDet.title?.capitalizedFirstLetter
        }
    }
    
    //MARK::- VIEW CYCLE
    
}
