//
//  UserDetailsTableViewCell.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class UserDetailsTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    
    @IBOutlet weak var labelInfo: UILabel!
    
    //MARK::- PROPERTIES
    var obj:Any? {
        didSet{
            guard let infoTo = obj as? AlbumsToDo else { return }
            labelInfo?.text = infoTo.title?.capitalizedFirstLetter
        }
    }
    
    //MARK::- VIEW CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
