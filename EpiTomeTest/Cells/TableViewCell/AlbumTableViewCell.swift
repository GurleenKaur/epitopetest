//
//  AlbumTableViewCell.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit



class AlbumTableViewCell: UITableViewCell {

    //MARK::- OUTLETS
    
    @IBOutlet weak var constraintHeightCollectionView: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK::- PROPERTIES
    var collectionViewDataSource : CollectionViewDataSource?
    var items = [Any]()
    var cellForItemAt: ListCellConfigureBlock?
    var cellForRowAt: ListCellConfigureBlock?
    var didSelectRow : DidSelectedRow?
    let albumVc = StoryboardScene.Main.albumViewController.instantiate()
    
    //MARK::- VIEW CYCLE
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    //MARK::- CONFIGURE COLLECTIONVIEW
    
    func configureCollectionView(){
        didSelectRow = { [ weak self ] indexPath in
            self?.albumVc.albumId = /(self?.items[indexPath.row] as? AlbumsToDo)?.id
            UIApplication.topViewController()?.pushVc(vc: self?.albumVc ?? UIViewController() )
        }
        
        cellForItemAt = { [ weak self ] ( cell , item , indexPath) in
            guard let cell = cell as? AlbumCollectionViewCell else { return }
            cell.album = item
        }
        
        collectionViewDataSource = CollectionViewDataSource(items: items, collectionView: collectionView, cellIdentifier: CellIdentifiers.albumCollectionViewCell.rawValue , headerIdentifier: "", cellHeight: 100, cellWidth: 120, minimumLineSpacing: 8.0, minimumInteritemSpacing:10.0, edgeInsetsMake: UIEdgeInsets.zero, sectionHeaderHeight: CGSize.zero)
        collectionViewDataSource?.configureCellBlock = cellForItemAt
        collectionViewDataSource?.aRowSelectedListener = didSelectRow
        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = collectionViewDataSource
        collectionView.reloadData()
    }
    
    //MARK::- FUNCTIONS
    
    //MARK::- ACTIONS

}
