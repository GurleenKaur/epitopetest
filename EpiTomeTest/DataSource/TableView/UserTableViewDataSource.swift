//
//  UserTableViewDataSource.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

class UserTableViewDataSource: TableViewDataSource {

    var posts = [Any]()
    var todos = [Any]()
    var album = [Any]()
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var identifier = ""
        
        var itm: Any?
        switch /indexPath.section {
        case 0:
            identifier =  CellIdentifiers.userDetailsTableViewCell.rawValue
            itm = posts[indexPath.row]
        case 2:
             identifier =  CellIdentifiers.userDetailsTableViewCell.rawValue
             itm = todos[indexPath.row]
        case 1:
             identifier = CellIdentifiers.albumTableViewCell.rawValue
        default:
             identifier = /cellIdentifier
        }
        let cell: UITableViewCell = tableView.dequeueReusableCell(withIdentifier: identifier , for: indexPath) as UITableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        (cell as? AlbumTableViewCell)?.items = self.album
        (cell as? AlbumTableViewCell)?.configureCollectionView()
        (cell as? AlbumTableViewCell)?.constraintHeightCollectionView.constant = 140
        if let block = self.configureCellBlock , let item: Any = itm{
            block(cell , item , indexPath)
        }
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return posts.count
            
        case 1:
            return 1
            
        case 2:
            return todos.count
            
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case 0:
            return "POSTS"
            
        case 1:
            return "ALBUMS"
            
        case 2:
            return "TODOs"
            
        default:
            return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.section == 1) ? 150 : UITableViewAutomaticDimension
    }
    
}
