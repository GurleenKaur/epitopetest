//
//  ViewControlle.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func configureChildViewController(childController: UIViewController, onView: UIView?) {
        var holderView = self.view
        if let onView = onView {
            holderView = onView
        }
        addChildViewController(childController)
        holderView?.addSubview(childController.view)
        //        childController.view.frame = (holderView?.frame) ?? CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height:  UIScreen.main.bounds.height )
        constrainViewEqual(holderView: holderView!, view: childController.view)
        childController.didMove(toParentViewController: self)
    }
    
    
    func constrainViewEqual(holderView: UIView, view: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false
        //pin 100 points from the top of the super
        let pinTop = NSLayoutConstraint(item: view, attribute: .top, relatedBy: .equal,
                                        toItem: holderView, attribute: .top, multiplier: 1.0, constant: 0)
        let pinBottom = NSLayoutConstraint(item: view, attribute: .bottom, relatedBy: .equal,
                                           toItem: holderView, attribute: .bottom, multiplier: 1.0, constant: 0)
        let pinLeft = NSLayoutConstraint(item: view, attribute: .left, relatedBy: .equal,
                                         toItem: holderView, attribute: .left, multiplier: 1.0, constant: 0)
        let pinRight = NSLayoutConstraint(item: view, attribute: .right, relatedBy: .equal,
                                          toItem: holderView, attribute: .right, multiplier: 1.0, constant: 0)
        
        holderView.addConstraints([pinTop, pinBottom, pinLeft, pinRight])
    }
    
    
    func presentVc(vc: UIViewController){
        self.navigationController?.present(vc, animated: true, completion: nil)
    }
    
    func pushVc(vc: UIViewController){
        self.navigationController?.pushViewController(vc, animated: true )
    }
    
    func popVc(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func presentVcOnSelf(vc: UIViewController){
        self.present(vc, animated: true, completion: nil)
    }
    
    func dismiss(animated: Bool){
        self.dismiss(animated: animated, completion: nil)
    }
    
    @discardableResult func addChildViewController(withChildViewController childViewController: UIViewController , view: UIView) -> UIViewController {
        // Add Child View Controller
        addChildViewController(childViewController)
        childViewController.beginAppearanceTransition(true, animated: true)
        // Add Child View as Subview
        view.addSubview(childViewController.view)
        // Configure Child View
        childViewController.view.frame = view.bounds
        childViewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        // Notify Child View Controller
        childViewController.didMove(toParentViewController: self)
        return childViewController
    }
    
    @discardableResult func removeChildViewController(withChildViewController childViewController: UIViewController , view: UIView ) -> UIViewController {
        // Notify Child View Controller
        childViewController.willMove(toParentViewController: nil)
        childViewController.beginAppearanceTransition(false, animated: true)
        // Remove Child View From Superview
        childViewController.view.removeFromSuperview()
        // Notify Child View Controller
        childViewController.removeFromParentViewController()
        return childViewController
    }
    
}
