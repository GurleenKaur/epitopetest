
//
//  StringExtension.swift
//  EpiTomeTest
//
//  Created by Harminder on 07/04/18.
//  Copyright © 2018 Gurleen. All rights reserved.
//

import UIKit

extension String {
    func toBoolVal() -> Bool {
        return self == "1" || self == "true" || self == "yes"
    }
    
    var capitalizedFirstLetter:String {
        let string = self
        return string.replacingCharacters(in: startIndex...startIndex, with: String(self[startIndex]).capitalized)
}
}


extension Bool{
    mutating func toggleVal() -> Bool{
        if self == true {
            self = false
            return self
        } else {
            self = true
            return self
        }
    }
}
